// In App.js in a new project

import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './src/screen/main'
import Student1Screen from './src/screen/Game.js';
import Student2Screen from './src/screen/Rugby.js';
import Student3Screen from './src/screen/Sun.js';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Game" component={Student1Screen} />
        <Stack.Screen name="Rugby" component={Student2Screen} />
        <Stack.Screen name="Sun" component={Student3Screen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
