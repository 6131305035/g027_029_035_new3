import * as React from 'react';
import { StyleSheet, View, Text, Image, ImageBackground } from 'react-native';

function Student3Screen() {
  return (
    
    <View style={{textAlign:"center"}}>
      

          <ImageBackground source={require('./img/44.gif')} style={styles.BG}>
              <Image source={{uri:'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/68393238_2368713746679184_5964456233967550464_o.jpg?_nc_cat=107&_nc_sid=dd7718&_nc_oc=AQm8kqR0Yie4AAy-64KOzRO-MQjkX8X69nV8XoHzlfLp9cyDOPnf1_4kRxRopREFkTk&_nc_ht=scontent.fbkk10-1.fna&oh=f92bb76a9f6cfd3192f50e0b4247b72a&oe=5EB63CC4'}} style={styles.ProfilePic}/>
              <Text style={styles.text}>Hello World ! !</Text>
              <Text style={styles.text2}>My name is Santirat Charin</Text>
              <Text style={styles.text}>My Student ID is 6131305029</Text>
          </ImageBackground> 

        

    </View>
  );
}
const styles = StyleSheet.create({

  text : {
    fontSize : 20,
    color : 'white',
    margin : 5
  },
  text2 : {
    fontSize : 25,
    color : 'white',
    margin : 5
  },
  ProfilePic : {
    width : 200,
    height : 200,
    borderRadius : 200 / 2,
    marginTop : '30%'
    
  },
  BG : {
      width: '100%', 
      height: 750, 
      alignItems : 'center'
  }
});

export default Student3Screen;