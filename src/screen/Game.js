import * as React from 'react';
import { View, Text, Image, StyleSheet, ImageBackground  } from 'react-native';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage,CardViewWithIcon } from 'react-native-cards';



function Student1Screen() {
  
  return (
    <View>
      <ImageBackground source={{uri:'https://media.giphy.com/media/HSY6sIl7suWg8/giphy.gif'}} style={styles.BG}>
            <View style={styles.CardProfile}>
                    

                      <View style={styles.ProfileMove}>
                        <Image source={{uri:'https://scontent.fbkk4-1.fna.fbcdn.net/v/t1.0-9/87342403_1228831157312245_2935409803771510784_o.jpg?_nc_cat=105&_nc_sid=85a577&_nc_eui2=AeFh38-rEPaQurJvYzNSchUMEGi35Bl96G0w_Lxo9aLvkxx-_H3HIWg2UT1KX3BDOCWxpfBXJWBhwKtwCBHMrzPhY1IkoHOALI5zDHmvFsEBnw&_nc_ohc=p1ZHkfj2esEAX8M80Sp&_nc_ht=scontent.fbkk4-1.fna&oh=a47582f1da99bb141ef1dff3385e2f75&oe=5EFE5E4F'}} style={styles.ProfilePic}/>
                        <Text style={styles.text}></Text>
                        <Text style={styles.text2}>KOMGRICH SRISAK</Text>
                        <Text style={styles.text}>ID : 613 1305 035</Text>
                        <Text style={styles.text3}>___________________________</Text>
  <Text style={styles.text3}>Current focus : </Text>
  <Text  style={styles.text3}>1. Software Architecture</Text>
  <Text  style={styles.text3}>2. 3 Main Data Expertise</Text>
  <Text style={styles.text3 }>3. AI software</Text>
  <Text style={styles.text3}>_______________________________</Text>
  <Text style={styles.text3}> Interested In all interdisciplinary subjects with computer Sciences  and software Engineerings
                        </Text>

                      </View>
                      

                    

            </View>
      </ImageBackground>
    </View>
  );
}
const styles = StyleSheet.create({

  text : {
    fontSize : 14,
    color : '#F9ECCC',
    margin : 5,
    
  },
  text2 : {
    fontSize : 20,
    color : '#F9ECCC',
    margin : 5
  },
  text3 : {
    fontSize : 14,
    color : '#F9ECCC',
    justifyContent : 'center',
    textAlign : "center"
  },
  ProfilePic : {
    width : 200,
    height : 200,
    borderRadius : 200 / 2,
    marginTop : '30%'
  },
  BG : {
      width: '100%', 
      height: 750, 
      alignItems : 'center'
  },
  ProfileMove : {
    flex : 1,
    alignItems : "center",
    

  },
  CardProfile :{
    flex : 1,
    height : 700,
    width : 300,
    alignItems : "center",
    justifyContent : "center",
    alignSelf : "center"
  },
  CardProfile_2 : {
    flex : 1,
    height : 500,
    width : 300,
    alignItems : "center",
    justifyContent : "center",
    alignSelf : "center"
  }
  
});

export default Student1Screen;
