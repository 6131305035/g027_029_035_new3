import * as React from 'react';
import { TouchableOpacity, View, Text ,Button, StyleSheet, ImageBackground} from 'react-native';

function MainScreen({ navigation }) {
  return (
    <View style={{ flex: 1}}>

      <ImageBackground source={{uri:'https://i.pinimg.com/originals/96/68/13/96681345928a4a4a6279e7f12c27c500.gif'}} style={styles.BG}>
      <Text style={styles.text}>Main</Text>
      <Text style={styles.text2}>Choose your player</Text>

          <View style = {{alignSelf : 'center'}}>  
          <TouchableOpacity onPress={() => navigation.navigate('Game')}>
               <ImageBackground source={require('./img/3.png')} style={styles.touch}>
                <Text style={styles.textbutton}>Player 1 "Game"</Text>
                </ImageBackground>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('Rugby')}>
               <ImageBackground source={require('./img/3.png')} style={styles.touch}>
                <Text style={styles.textbutton}>Player 2 "Rugby"</Text>
                </ImageBackground>
          </TouchableOpacity>
            
          <TouchableOpacity  onPress={() => navigation.navigate('Sun')}>
               <ImageBackground source={require('./img/3.png')} style={styles.touch}>
                <Text style={styles.textbutton}>Player 3 "Sun"</Text>
                </ImageBackground>
          </TouchableOpacity>
          </View>
          </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({

  button:{
    flex : 1,
    padding : 30,
    marginHorizontal : 100,
  },
  BG : {
    width: '100%', 
    height: 750, 
    alignItems : 'center'
},
text : {
  fontSize : 20,
    color : 'white',
    margin : 5,
    marginTop : '25%'
},
text2 : {
  fontSize : 20,
    color : 'white',
    margin : 5
},
textbutton : {

  color : 'black'
},
touch : {
 alignItems : 'center',
 flexDirection : 'row',
 justifyContent : 'center',
 width : 180,
 height : 45
}

});

export default MainScreen;
